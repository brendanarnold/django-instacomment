# InstaComment #

**Less than alpha!**

A simple way to include comments in your webpage. 

## Features ##

- Multiple OAuth providers for sign in e.g. Google, Facebook, Microsoft Live, Yahoo etc.
- Single Javascript include in page then a single html div  wherever you want to comments to appear.
- Templates can be easily overridden in the page itself.
- Lightweight access to other data such as number of comments for insertion in e.g. blog index pages.
- Backend easily deploys on Windows Azure for scalability and ease of maintenance
- Unintrusive JavaScript, RequireJS is the only global name imported.
- Browseable API for the backend.

## Technologies ##

- Front-end
	- BackboneJS
	- RequireJS
	- jQuery
	- UnderscoreJS
- Back-end
	- Django
	- Python-Social-Auth
	- Django-Rest-Framework 	