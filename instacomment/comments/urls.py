from django.conf.urls import patterns, include, url
from comments import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'comments', views.CommentViewSet)

urlpatterns = patterns('',
    url(r'^$', 'comments.views.api_root'), 

    url(r'^', include(router.urls)),
    
    url(r'^clear-user-session/', 
        'comments.views.logout_view', 
        name='clear-user-session'),
    
    url(r'^page-comments/(?P<pk>[\w-]+)/$',
        views.PageCommentsView.as_view(),
        name='page-comments'),
    
    url(r'^comment-counts/',
        views.CommentCountsView.as_view(),
        name='comment-counts'),
    
    url(r'^oauth-providers/', 
        views.ProviderListView.as_view(), 
        name='oauth-providers'),
    
    url(r'^current-user/',
        views.CurrentUserView.as_view(),
        name='current-user'),
    url(r'^logged-in/',
        views.loggedin_view),

    # Intercept calls to social auth to save redirect URL to session, 
    # passes onwards afterwards
    url(r'^login/(?P<backend>[^/]+)/$', views.wrap_social_auth),

    url(r'', 
        include('social.apps.django_app.urls', 
        namespace='social')),

)
