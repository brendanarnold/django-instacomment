from rest_framework import serializers
from comments import models
from django.contrib.auth.models import User



class CommentSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.Field(source='owner.username')
    class Meta:
        model = models.Comment
        fields = ('url', 'content', 'created', 'owner', 'pageid')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    comments = serializers.HyperlinkedRelatedField(many=True, view_name='comment-detail')
    is_authenticated = serializers.BooleanField(source='is_authenticated', read_only=True)
    class Meta:
        model = User
        fields = ('url', 'username', 'comments', 'first_name', 'last_name', 'is_authenticated')

