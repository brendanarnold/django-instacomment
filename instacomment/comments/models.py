from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    content = models.TextField()
    pageid = models.CharField(max_length = 200)
    # TODO: Here will have to change to general User model
    owner = models.ForeignKey('auth.User', related_name = 'comments')
    class Meta:
        ordering = ('created', )