from rest_framework import viewsets, status
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.auth import logout
from comments import models, serializers
from comments.permissions import IsOwnerOrReadOnly
from comments.serializers import CommentSerializer
from django.http import HttpResponseRedirect
import instacomment.settings as settings
from social.backends.utils import load_backends
import social.apps.django_app.views
from django.shortcuts import redirect
import bleach


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'comments': reverse('comment-list', request=request, format=format),
        'oauth-providers': reverse('oauth-providers', request=request, format=format),
        'current-user': reverse('current-user', request=request, format=format),
        'page-comments': reverse('page-comments', ('some-example-page-id', ), request=request, format=format),
        'comment-counts': reverse('comment-counts', request=request, format=format),
    })

def loggedin_view(request):
    '''
    Redirect back to the app once logged in
    '''
    if request.session.has_key('client_page_url'):
        url = request.session['client_page_url']
    else:
        # Best fallback is to use the client host although this could be annoying
        url = '//' + settings.CLIENT_HOSTNAME
    return redirect(url)

# A hack to save the referring uri to the session so that loggedin_view
# can bounce back to the right client page
def wrap_social_auth(request, backend):
    if request.META.has_key('HTTP_REFERER'):
        request.session['client_page_url'] = request.META['HTTP_REFERER']
    else:
        request.session['client_page_url'] = '//' + settings.SERVER_HOSTNAME
    return social.apps.django_app.views.auth(request, backend)


def logout_view(request):
    '''
    Performs Django logout, e.g. deletes user object in the session.
    '''
    logout(request)
    return HttpResponseRedirect("/")

class CommentCountsView(APIView):
    '''
    Returns all the necessary data to include comment counts in a page
    '''
    def get(self, request, *args, **kwargs):
        data = {}
        data['pageid-counts'] = {}
        comments = models.Comment.objects.all()
        for comment in comments:
            if not data['pageid-counts'].has_key(comment.pageid):
                data['pageid-counts'][comment.pageid] = 1
            else:
                data['pageid-counts'][comment.pageid] += 1
        return Response(data, status=status.HTTP_200_OK)

class PageCommentsView(APIView):
    '''
    Returns all necessary data to render a comments page
    '''
    def get(self, request, pk, *args, **kwargs):
        data = {}
        # Get the provider data
        backends = load_backends(settings.AUTHENTICATION_BACKENDS)
        data['oauth-providers'] = []
        for name, backend in backends.items():
            try:
                extra_provider_data = settings.INSTACOMMENT_PROVIDER_DETAILS[name]
            except KeyError:
                raise Exception("INSTACOMMENT_PROVIDER_DETAILS does not contain and entry for %s" % name)
            provider_data = {'name': name, 'login_url' : '//%s/login/%s/' % (settings.SERVER_HOSTNAME, name)}
            data['oauth-providers'].append(dict(provider_data.items() + extra_provider_data.items()))
        # Now get current user data
        if (isinstance(request.user, AnonymousUser)):
            data['current-user'] = { 
                'url': '',
                'username': '',
                'comments': [],
                'first_name': '',
                'last_name': '',
                'is_authenticated': False,
            }
        else:
            data['current-user'] = serializers.UserSerializer(request.user).data
        # Get the comments for the page
        data['comments'] = [serializers.CommentSerializer(c).data for c in models.Comment.objects.filter(pageid = pk)]
        # Now send it all off
        return Response(data, status=status.HTTP_200_OK)

class ProviderListView(APIView):
    '''
    Return a list of configured OAuth provider objects including the login_url
    '''
    def get(self, request, *args, **kwargs):
        backends = load_backends(settings.AUTHENTICATION_BACKENDS)
        data = []
        for name, backend in backends.items():
            try:
                extra_provider_data = settings.INSTACOMMENT_PROVIDER_DETAILS[name]
            except KeyError:
                raise Exception("INSTACOMMENT_PROVIDER_DETAILS does not contain and entry for %s" % name)
            provider_data = {'name': name, 'login_url' : '//%s/login/%s/' % (settings.SERVER_HOSTNAME, name)}
            data.append(dict(provider_data.items() + extra_provider_data.items()))
        return Response(data, status=status.HTTP_200_OK)

class CurrentUserView(APIView):
    '''
    Returns the current user, logged in or not
    '''
    def get(self, request, *args, **kwargs):
        if (isinstance(request.user, AnonymousUser)):
            data = { 
                'url': '',
                'username': '',
                'comments': [],
                'first_name': '',
                'last_name': '',
                'is_authenticated': False,
            }
        else:
            data = serializers.UserSerializer(request.user).data
        return Response(data)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    Provides 'list' and 'detail'
    '''
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer



class CommentViewSet(viewsets.ModelViewSet):
    '''
    Provides 'list', 'create', 'retrieve', 'update', 'destroy'
    '''
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    # TODO: Re-enable when sorted out front-end
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)
    def pre_save(self, obj):
        obj.owner = self.request.user
        obj.content = bleach.clean(self.request.DATA['content'], attributes=settings.WHITELIST_HTML_ATTRIBUTES)
