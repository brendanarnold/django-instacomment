from rest_framework import permissions



class IsOwnerOrReadOnly(permissions.BasePermission):
    '''
    Custom permission to only allow the owner to edit an object
    '''
    def has_object_permission(self, request, view, obj):
        # If read-only requested then grant
        if request.method in permissions.SAFE_METHODS:
            return True
        # If write requested then grant if owner
        return obj.owner == request.user