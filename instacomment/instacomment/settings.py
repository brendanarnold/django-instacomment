# Django settings for instacomment project.
import os


DEBUG = False
TEMPLATE_DEBUG = DEBUG

# Should you use Azure production server when running locally
# N.b. May need to add your current IP when connecting to remote Azure DB. This is under the Azure SQL database under 'configure'
USE_AZURE_DB_FOR_DEV = True

PROJECT_ROOT = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

if os.getenv('IS_DEV', None) is None:
    SERVER_HOSTNAME = "sdf-instacomment.azurewebsites.net"
    CLIENT_HOSTNAME = "brendan.sdf-eu.org"
else:
    SERVER_HOSTNAME = "localhost"
    CLIENT_HOSTNAME = "localhost"

ADMINS = (
    ('Brendan Arnold', 'brendanarnold@gmail.com'),
)

MANAGERS = ADMINS

# Need to set IS_DEV loal environment variable on development machine - use to determine if deployed or not
if (os.getenv('IS_DEV', None) is None) or USE_AZURE_DB_FOR_DEV:
    DATABASES = {
        'default': {
            'ENGINE': 'sql_server.pyodbc', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'sdf-instacomments',                      # Or path to database file if using sqlite3.
            'USER': 'azure@wmwxbbskl4',                      # Not used with sqlite3.
            'PASSWORD': os.getenv('DB_PASSWORD', None),                  # Not used with sqlite3.
            'HOST': 'wmwxbbskl4.database.windows.net',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
            'OPTIONS': {
                'driver': 'SQL Server Native Client 11.0',
                'MARS_Connection': True, 
            },
        }
    }
else:
   DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': os.path.join(PROJECT_ROOT, 'devdb.sqlite'),                      # Or path to database file if using sqlite3.
            'USER': '',                      # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        }
    }

# Add the domain hosting the webpage here

ALLOWED_HOSTS = (
    SERVER_HOSTNAME,
)

CORS_ORIGIN_WHITELIST = (
    CLIENT_HOSTNAME,
  #  'localhost:9000',
)
CORS_ALLOW_CREDENTIALS = True

SOCIAL_AUTH_USER_MODEL = 'auth.User'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/logged-in/'
#SOCIAL_AUTH_LOGIN_ERROR_URL = '/login-error/'
#SOCIAL_AUTH_LOGIN_URL = '/login-url/'
#SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/new-users-redirect-url/'
#SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/new-association-redirect-url/'
#SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/clearusersession/'
##SOCIAL_AUTH_INACTIVE_USER_URL = '/inactive-user/'
#SOCIAL_AUTH_UID_LENGTH = 223
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True

INSTACOMMENT_PROVIDER_DETAILS = {
    'google-oauth2': {
        'real_name': 'Google',
        'img_url': 'http://placehold.it/100x100', 
    }
}

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = "435168336971-gs73edcll9l9n04preku6j424qkr6ejt.apps.googleusercontent.com"
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "Ecp4WzeOH-0riDOW6Ahxdoqg"
GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS =  { 'access_type': 'online', 'approval_prompt': 'auto' }


# Sanction user model
AUTHENTICATION_BACKENDS = (
    'social.backends.google.GoogleOAuth2',
    #'django.contrib.auth.backends.ModelBackend',
)


WHITELIST_HTML_ATTRIBUTES = {
    'p': [],
    'a': ['href'],
    'code': [],
    'img': ['src', 'alt'],
    'b': [],
    'i': [],
    'pre': [],
    'strong': [],
    'em': [],
}

TEMPLATE_CONTEXT_PROCESSORS = (
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)
   
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"

# TODO: Set this to site root static dir when development is over
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\', '/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n(bd1f1c%e8=_xad02x5qtfn%wgwpi492e$8_erx+d)!tpeoim'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'instacomment.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'instacomment.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'comments',
    'rest_framework',
    'social.apps.django_app.default',
    'corsheaders',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
