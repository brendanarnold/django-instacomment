from django.conf.urls import url, patterns, include


urlpatterns = patterns('',
    url(r'^', include('comments.urls')),
)