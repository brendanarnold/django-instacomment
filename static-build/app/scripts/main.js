/*global require*/
'use strict';

require([
    'backbone',
    'jquery-extended',
    'config',
    'views/comment-page-data',
    'views/index-page-data',
], function (Backbone, $, cfg, CommentPageDataView, IndexPageDataView) {

    Backbone.history.start();
    $(document).ready(function() {
        // Create and render a view for each comments div
        // jQuery selector for class name startswith
        // SELECTOR_STEM_COMMENTS
        $('[class^=' + cfg.CLASS_STEM_COMMENTS + ']').each(function() {
            new CommentPageDataView({ 'el': this });
        });
        new IndexPageDataView();
    });


});
