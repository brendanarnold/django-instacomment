/*global define*/

define([
    'underscore',
    'backbone',
    'templates',
    'jquery-extended',
    'models/comment-page-data',
    'views/edit-comment',
    'utils',
    'config',
], function (_, Backbone, JST, $, CommentPageDataModel, EditCommentView, utils, cfg) {
    'use strict';

    var CommentPageDataView = Backbone.View.extend({
        'template': utils.getTemplate(JST['app/scripts/templates/comment-page-data.ejs'], cfg.TEMPLATE_OVERRIDE_LIST),
        'initialize': function(opts) {
            this.el = opts.el;
            this.pageId = utils.extractClassAppendedIdFromElement(this.$el, cfg.CLASS_STEM_COMMENTS);
            this.model = new CommentPageDataModel({ 'pageId': this.pageId });
            // Stitch on the model events
            this.listenTo(this.model, 'change', this.render);
            // this.listenTo(this.model, 'all', function(name) { console.log(name); });
            this.model.fetch();
        },
        'editCommentView': null,
        'render': function() {
            var renderedTmpl = this.template({ 'data': this.model, 'cfg': cfg });
            this.$el.html(renderedTmpl);
            var childFormContainer = $(this.$el).find('.' + cfg.CLASS_EDIT_FORM_CONTAINER);
            this.editCommentView = new EditCommentView({
                    'el' : childFormContainer,
                    'pageId': this.pageId,
                    'pageComments': this.model,
            });
            this.editCommentView.render();
        },

    });

    return CommentPageDataView;
});
