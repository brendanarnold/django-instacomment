/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'utils',
    'config',
    'models/comment',
], function ($, _, Backbone, JST, utils, cfg, CommentModel) {
    'use strict';

    var EditCommentView = Backbone.View.extend({
        'initialize': function(opts) {
            this.pageComments = opts.pageComments;
            this.pageId = opts.pageId;
            this.el = '.' + cfg.CLASS_EDIT_FORM_CONTAINER;
        },
        'template': utils.getTemplate(JST['app/scripts/templates/edit-comment.ejs'], cfg.TEMPLATE_OVERRIDE_FORM),
        'pageComments': null,
        'pageId': null,
        'render': function() {
            // TODO: Future version will require the model to edit existing
            // comments
            var renderedTmpl = this.template({ 'pageid': this.pageId, 'cfg': cfg });
            this.$el.append(renderedTmpl);
            return this;
        },
        'events': {
            'submit': 'saveComment',
        },
        'saveComment': function(ev) {
            var that = this;
            ev.preventDefault();
            var form = $(ev.currentTarget).find('.' + cfg.CLASS_EDIT_FORM);
            // Serialize and send off data
            var commentJson = form.serializeObject();
            var commentModel = new CommentModel();
            // Diable form before sending (has to be done after
            // serializing)
            this.disableForm();
            commentModel.save(commentJson, {
                'success': function(comment) {
                    console.log(comment);
                    that.enableForm();
                    that.pageComments.addComment(comment.attributes);
                },
                'error': function(model, xhr, options) {
                    that.enableForm();
                    if (xhr.status === 403) {
                        // Not authenticated
                        // Shouldn't see this
                    }
                },
            });
        },
        'enableForm' : function() {
            // Renable the form after the request takes place
            this.$el.find('textarea').attr({ 'disabled' : false });
            this.$el.find('button').attr({ 'disabled' : false });
        },
        'disableForm' : function() {
            // Disable the form whilst the request takes place
            this.$el.find('textarea').attr({ 'disabled' : true });
            this.$el.find('button').attr({ 'disabled' : true });
        },
        
    });

    return EditCommentView;
});
