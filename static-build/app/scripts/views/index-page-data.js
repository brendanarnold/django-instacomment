/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'utils',
    'config',
    'models/index-page-data'
], function ($, _, Backbone, utils, cfg, IndexPageDataModel) {
    'use strict';

    var IndexPageDataView = Backbone.View.extend({
        'initialize': function(opts) {
            this.model = new IndexPageDataModel();
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
        },
        'render': function() {
            // Take each container for counts and substitute in the
            // counts from the backend API
            var that = this;
            $('[class^=' + cfg.CLASS_STEM_COUNT + ']').each(function(i) {
                var container = $(this);
                var pageId = utils.extractClassAppendedIdFromElement(container, cfg.CLASS_STEM_COUNT);
                var count = that.model.get('pageid-counts')[pageId];
                container.html(count);
            });
        },
        
    });

    return IndexPageDataView;
});
