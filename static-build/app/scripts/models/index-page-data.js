/*global define*/

define([
    'underscore',
    'backbone',
    'config'
], function (_, Backbone, cfg) {
    'use strict';

    var IndexPageDataModel = Backbone.Model.extend({
        'url': cfg.API_ENDPOINT_INDEX_PAGE,
    });

    return IndexPageDataModel;
});
