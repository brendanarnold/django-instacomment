/*global define*/

define([
    'underscore',
    'backbone',
    'config',
], function (_, Backbone, cfg) {
    'use strict';

    var CommentModel = Backbone.Model.extend({
        defaults: {
        },
        'url': cfg.API_ENDPOINT_COMMENTS,
    });

    return CommentModel;
});
