'use strict';
// Own build variables
var REQUIRE_FN = 'app/bower_components/requirejs/require.js';
var JS_BUILD_FN = 'build/instacomment.js';
var JS_DEPLOY_FN = '../instacomment/comments/static/instacomment.js';


var LIVERELOAD_PORT = 35729;
var SERVER_PORT = 9000;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to match all subfolders:
// 'test/spec/**/*.js'
// templateFramework: 'lodash'

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // configurable paths
    var yeomanConfig = {
        app: 'app',
        dist: 'dist'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        watch: {
            options: {
                nospawn: true,
                livereload: true
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                    '<%= yeoman.app %>/*.html',
                    '{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css',
                    '{.tmp,<%= yeoman.app %>}/scripts/{,*/}*.js',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                    '<%= yeoman.app %>/scripts/templates/*.{ejs,mustache,hbs}',
                    'test/spec/**/*.js'
                ]
            },
            jst: {
                files: [
                    '<%= yeoman.app %>/scripts/templates/*.ejs'
                ],
                tasks: ['jst']
            },
            test: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.js', 'test/spec/**/*.js'],
                tasks: ['test']
            }
        },
        connect: {
            options: {
                port: SERVER_PORT,
                // change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    port: 9001,
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, 'test'),
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                path: 'http://localhost:<%= connect.options.port %>'
            }
        },
        clean: {
            dist: ['.tmp', '<%= yeoman.dist %>/*'],
            server: '.tmp'
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/scripts/{,*/}*.js',
                '!<%= yeoman.app %>/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },
        requirejs: {
            dist: {
                // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
                options: {
                    // `name` and `out` is set by grunt-usemin
                    // Had to include this rather than the config file
                    // since the config is not simple JSON format
                    // dir: '<%= yeoman.app %>/scripts',
                    baseUrl: '<%= yeoman.app %>/scripts',
                    // dir: 'build',
                    optimize: 'none',
                    paths: {
                        'jquery': '../bower_components/jquery/jquery',
                        // Patch jQuery using own module code
                        'jquery-extended': '../scripts/jquery-extended',
                        'jquery-cookie': '../bower_components/jquery-cookie/jquery.cookie',
                        'backbone': '../bower_components/backbone/backbone',
                        'underscore': '../bower_components/underscore/underscore',
                        'bootstrap': '../scripts/vendor/bootstrap',
                        'order': '../bower_components/require-order/order',
                        'templates': '../../.tmp/scripts/templates',
                        'require': '../bower_components/requirejs/require.js'
                    },
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true,
                    name: 'main',
                    out: JS_BUILD_FN,
                    shim: {
                        underscore: {
                            exports: '_'
                        },
                        backbone: {
                            deps: [
                                'underscore',
                                'jquery-extended'
                            ],
                            exports: 'Backbone'
                        },
                        bootstrap: {
                            deps: ['jquery'],
                            exports: 'jquery-extended'
                        }
                    }    
                    // mainConfigFile: 'app/scripts/main.js'
                }
            }
        },
        bower: {
            all: {
                rjsConfig: '<%= yeoman.app %>/scripts/main.js'
            }
        },
        jst: {
            options: {
                amd: true
            },
            compile: {
                files: {
                    '.tmp/scripts/templates.js': ['<%= yeoman.app %>/scripts/templates/*.ejs']
                }
            }
        },
        concat: {
            dist: {
                src: [
                    REQUIRE_FN,
                    JS_BUILD_FN
                ],
                dest: JS_DEPLOY_FN,
                separator: ";",
            }
        }
    });

    grunt.registerTask('createDefaultTemplate', function () {
        grunt.file.write('.tmp/scripts/templates.js', 'this.JST = this.JST || {};');
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open', 'connect:dist:keepalive']);
        }

        if (target === 'test') {
            return grunt.task.run([
                'clean:server',
                'createDefaultTemplate',
                'jst',
                'connect:test',
                'watch:livereload'
            ]);
        }

        grunt.task.run([
            'build',
            'connect:livereload',
            'open',
            'watch'
        ]);
    });

    grunt.registerTask('test', [
        'clean:server',
        // 'coffee',
        'createDefaultTemplate',
        'jst',
        // 'compass',
        'connect:test',
        // 'mocha',
        'watch:test'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        // 'coffee',
        'createDefaultTemplate',
        'jst',
        // 'compass:dist',
        // 'useminPrepare',
        'requirejs',
        // 'imagemin',
        // 'htmlmin',
        'concat',
        // 'cssmin',
        // 'uglify',
        // 'copy',
        // 'rev',
        // 'usemin'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'test',
        'build'
    ]);
};

