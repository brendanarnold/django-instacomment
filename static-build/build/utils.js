define([
    'config',
    'underscore'
], function(cfg, _) {
    'use strict';

    // A function that returns the overriden template or the default if
    // specified
    function getTemplate(dflt, selector) {
        var tmpl = $(selector);
        if (tmpl.length) {
            return tmpl.html();
        } else {
            return dflt;
        }
    }

    // Id extracted from class which may exist alongside other
    // classes. Use a stem to match the class then chop it off to obtain
    // the id i.e. class="<stem><id> someotherclassname" -> <id>
    function extractClassAppendedIdFromElement(el, classStem) {
        var regEx = new RegExp('^' + classStem, 'g');
        var className = _.find(el.attr('class').split(' '), function(c) {
            return c.match(regEx) !== null;
        });
        return className.slice(classStem.length);
    }

    return {
        'extractClassAppendedIdFromElement': extractClassAppendedIdFromElement,
        'getTemplate': getTemplate,
    };


});
