/*global define*/

// Patch up jQuery here with own includes - use the order plugin to
// specify order of loading

define([
    'order!jquery',
    'config',
    'order!jquery-cookie',
], function ($, cfg) {
    'use strict';

    // Configure AJAX to use the Instacomment host for queries
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
        // Documented at https://docs.djangoproject.com/en/dev/ref/contrib/csrf/#ajax
        var csrftoken = $.cookie('csrftoken');
        jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        // This is from Backbone Tutorial video
        options.url = cfg.INSTACOMMENT_HOST + options.url;
        // This is from somewhere else
        options.xhrFields = {
            withCredentials: true,
        };
    });

    // Extend jQuery to enable serializing form input into JSON
    // From BackboneTutorial
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if(!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    




    return $;
});
