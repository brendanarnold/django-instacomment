/*global define*/

define([
    'underscore',
    'backbone',
    'config',
], function (_, Backbone, cfg) {
    'use strict';

    var CommentPageDataModel = Backbone.Model.extend({
        'initialize': function(opts) {
            this.url = cfg.API_ENDPOINT_COMMENTS_PAGE + opts.pageId + '/';
        },
        'url': null,
        'addComment': function(comment) {
            // Create copy of array so will be recognised as a change by
            // the set function
            var comments = this.get('comments').slice();
            comments.push(comment);
            this.set({ 'comments': comments });
        },
    });

    return CommentPageDataModel;
});
