/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var IndexPageDataModel = Backbone.Model.extend({
        defaults: {
        }
    });

    return IndexPageDataModel;
});