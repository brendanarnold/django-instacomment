'use strict';

define(function(){

    // The class name stems for HTML injection, will populate
    // with comments list or comment count. Should be suffixed with pageid e.g.
    // <div class="instacomment-comments-someuniqueid"></div>
    var _CLASS_STEM_COMMENTS = 'instacomment-comments-';
    var _CLASS_STEM_COUNT = 'instacomment-count-';
    // Normalselectors for HTML injection
    var _CLASS_LOGIN_OPTIONS_AND_EDIT_FORM_CONTAINER = 'instacomment-login-edit-container';
    var _CLASS_LOGIN_OPTIONS_CONTAINER = 'instacomment-login-options-container';
    var _CLASS_EDIT_FORM_CONTAINER = 'instacomment-edit-form-container';
    var _CLASS_EDIT_FORM = 'instacomment-edit-form';

    // Ids which if specified in the HTML will override the default
    // template
    var _TEMPLATE_OVERRIDE_FORM = '#instacomment-comment-form-template';
    var _TEMPLATE_OVERRIDE_LIST = '#instacomment-comment-list-template';

    // If the Django app changes these might need to be altered
    var _STATIC_DIR_STEM = '/static/comments/';
    var _API_ENDPOINT_COMMENTS_PAGE = '/page-comments/';
    var _API_ENDPOINT_USERS = '/users/';
    var _API_ENDPOINT_COMMENTS = '/comments/';
    var _API_ENDPOINT_CURRENT_USER = '/current-user/';
    var _API_ENDPOINT_OAUTH_PROVIDERS = '/oauth-providers/';
    var _API_ENDPOINT_INDEX_PAGE = '/comment-counts/';

    // Settings calculated from other settings. This is why the whole
    // set of settings has to be typed out for the export below
    var _STATIC_BASE = require.toUrl('');
    // _INSTACOMMENT_HOST = _STATIC_BASE.substring(0, _STATIC_BASE.length - _STATIC_DIR_STEM.length);
    var _INSTACOMMENT_HOST = '//localhost:8181';

    return {
        'CLASS_STEM_COUNT': _CLASS_STEM_COUNT,
        'CLASS_STEM_COMMENTS': _CLASS_STEM_COMMENTS,
        'CLASS_LOGIN_OPTIONS_AND_EDIT_FORM_CONTAINER': _CLASS_LOGIN_OPTIONS_AND_EDIT_FORM_CONTAINER,
        'CLASS_LOGIN_OPTIONS_CONTAINER': _CLASS_LOGIN_OPTIONS_CONTAINER,
        'CLASS_EDIT_FORM_CONTAINER': _CLASS_EDIT_FORM_CONTAINER,
        'CLASS_EDIT_FORM': _CLASS_EDIT_FORM,
        'TEMPLATE_OVERRIDE_FORM': _TEMPLATE_OVERRIDE_FORM,
        'TEMPLATE_OVERRIDE_LIST': _TEMPLATE_OVERRIDE_LIST,
        'STATIC_DIR_STEM': _STATIC_DIR_STEM,
        'API_ENDPOINT_COMMENTS_PAGE': _API_ENDPOINT_COMMENTS_PAGE,
        'API_ENDPOINT_USERS': _API_ENDPOINT_USERS,
        'API_ENDPOINT_COMMENTS': _API_ENDPOINT_COMMENTS,
        'API_ENDPOINT_CURRENT_USER': _API_ENDPOINT_CURRENT_USER,
        'API_ENDPOINT_OAUTH_PROVIDERS': _API_ENDPOINT_OAUTH_PROVIDERS,
        'API_ENDPOINT_INDEX_PAGE': _API_ENDPOINT_INDEX_PAGE,
        'STATIC_BASE': _STATIC_BASE,
        'INSTACOMMENT_HOST': _INSTACOMMENT_HOST,
    };


});
