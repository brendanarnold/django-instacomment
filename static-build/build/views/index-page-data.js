/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var IndexPageDataView = Backbone.View.extend({
        template: JST['app/scripts/templates/index-page-data.ejs']
    });

    return IndexPageDataView;
});
